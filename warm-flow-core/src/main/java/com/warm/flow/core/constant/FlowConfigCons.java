package com.warm.flow.core.constant;

/**
 * warm-flow配置文件常量
 *
 * @author warm
 */
public class FlowConfigCons {

    /**
     * 是否支持任意跳转
     */
    public static final String BANNER = "warm-flow.banner";

    /**
     * 是否支持任意跳转
     */
    public static final String DATAFILLHANDLEPATH = "warm-flow.data-fill-handler-path";



}
